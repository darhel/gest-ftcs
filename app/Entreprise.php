<?php

namespace App;

use App\Traits\CanUpload;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Spatie\Activitylog\Contracts\Activity;
use Spatie\Activitylog\Traits\LogsActivity;

class Entreprise extends Model
{
    protected $guarded=[];
    protected $appends =["hasLogo"];
    use CanUpload, LogsActivity;

    protected static $logAttributes = ["nom_commercial","logo"];
    protected static $logName = 'entreprise';
    protected static $logOnlyDirty = true;
    protected static $submitEmptyLogs = false;


    protected $storage_path ="public/";




    public function tapActivity(Activity $activity, string $eventName)
    {
        $activity->description = "{$eventName}";
        if($eventName=="deleted")
        {
            $activity->as_yourself = "Vous avez supprimé l'entreprise <strong>{$this->nomComplet}</strong>";
            $activity->as_someone_else = ucFirst(Auth::check() ? Auth::user()->name : "Le système")." a supprimé le Projet <strong>{$this->nomComplet}</strong>";
        }
        elseif($eventName=="updated")
        {
            $activity->as_yourself = "Vous avez modifié l'entreprisel'entreprise <strong>{$this->nomComplet}</strong>";
            $activity->as_someone_else = ucFirst(Auth::check() ? Auth::user()->name : "Le système")." a modifié le Projet <strong>{$this->nomComplet}</strong>";
        }
        else
        {
            $activity->as_yourself = "Vous avez ajouté le Projet <strong>{$this->nomComplet}</strong>";
            $activity->as_someone_else = ucFirst(Auth::check() ? Auth::user()->name : "Le système")." a ajouté le Projet <strong>{$this->nomComplet}</strong>";
        }

    }

    public function getHasLogoAttribute()
    {
        if($this->logo == null) return false;
        return file_exists(storage_path("/app/public/featured/".$this->logo)) ? true : false;
    }
    public function getPathAttribute()
    {
        return 'storage/app/public/documents/permis/'.$this->permis_conduire;
    }

    public function getHasFileAttribute()
    {
        return file_exists(storage_path("/app/public/documents/permis/".$this->permis_conduire));
    }

        /**
     * Get the Projet's full name.
     *
     * @return string
     */
    public function getNomCompletAttribute()
    {
        return ucfirst($this->prenom)." ".ucfirst($this->nom);
    }


    public function scopeSearch($query, $q)
    {
        if ($q == null) return $query;
        return $query->orWhere('entreprises.nom_commercial', 'LIKE', "%{$q}%");
    }
    public function scopePays($query, $q)
    {
        if ($q == null) return $query;
        return $query->where('entreprises.pays_id',$q);

    }
    public function scopeFormeJuridique($query,$q)
    {
        if ($q == null) return $query;
        return $query->where('entreprises.forme_juridique','LIKE',$q);

    }
    public function scopeSecteur($query, $q)
    {
        if ($q == null) return $query;
        return $query->where('entreprises.secteur_activite_id',$q);

    }

    public function scopeSearchAsParameter($query, $q)
    {
        // dump($query);
        if ($q == null) return $query;
        return $query->orWhere('entreprises.nom_commercial', 'LIKE', "%{$q}%");
    }

    public function secteur()
    {
        return $this->belongsTo("App\SecteurActivite","secteur_activite_id");

    }

    public function responsable()
    {
        return $this->belongsTo("App\User","responsable_id");

    }

    public function pays()
    {
        return $this->belongsTo("App\Pays","pays_id");

    }

        /**
     * Get the owning commentable model.
     */
    public function creatable()
    {
        return $this->morphTo();
    }

    //Fichier
    public function fichiers()
    {
        return $this->hasMany('App\Fichier', 'entreprise_id');
    }

}
