<?php

namespace App\Http\Controllers\API;

use App\Client;
use App\Events\CashAdvancedHasBeenMadeEvent;
use App\Exports\LocationExport;
use App\Projet;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProjetRequest;
use App\StatutPayement;
use Carbon\Carbon;
use Illuminate\Http\Request;
use DB;
use Log;
use Excel;
use Illuminate\Support\Facades\Auth;

class ProjetController extends Controller
{

    public function __construct()
    {
        $this->middleware("auth");
    }


    public function index()
    {
        $per = request()->query("per_page") && is_numeric(request()->query("per_page")) ? request()->query("per_page"):10;

        return   Projet::with(['entreprise','creatable'])->orderBy("created_at",'desc')->paginate($per);
    }

    public function create()
    {
        //
    }


    public function store(ProjetRequest $request)
    {
        try
        {
            // $request->validate();

            DB::beginTransaction();

            if($request->input("description") != NULL){
                $description = $request->input("description");
            }else{
                $description = "Aucune description pour ce projet";
            }

            $projet = Projet::create(
                [
                    'titre' =>$request->input('titre'),
                    'budget' =>$request->input("budget"),
                    'date_debut_prev' =>$request->input("date_debut_prev"),
                    'date_fin_prev' =>$request->input("date_fin_prev"),
                    'avancement' =>$request->input('avancement'),
                    'responsable_id' =>$request->input("responsable_id"),
                    'entreprise_id' =>$request->input('entreprise_id'),
                    'description' => $description,
                    'creatable_type'=>"App\User",
                    'creatable_id'=>Auth::user()->id,
                ]
                );

            DB::commit();
            return response()->json(['success' => true,'projet'=> $projet]);

        }
        catch(\Exception $e)
        {
            DB::rollback();
            return ['status'=>false,'message'=>$e->getMessage()];
        }
    }

    public function show(Location $location)
    {

    }

    public function edit(Location $location)
    {

    }

    public function update(ProjetRequest $request, Projet $projet)
    {
        try
        {
            DB::beginTransaction();

            if($request->input("description") != NULL){
                $description = $request->input("description");
            }else{
                $description = "Aucune description pour ce projet";
            }

            $projet->titre = $request->input('titre');
            $projet->budget = $request->input("budget");
            $projet->date_debut_prev = $request->input("date_debut_prev");
            $projet->date_fin_prev = $request->input("date_fin_prev");
            $projet->avancement = $request->input('avancement');
            $projet->responsable_id = $request->input("responsable_id");
            $projet->entreprise_id = $request->input('entreprise_id');
            $projet->description = $description;
            $projet->creatable_type = "App\User";
            $projet->creatable_id= Auth::user()->id;

            $projet->save();

            DB::commit();
            return response()->json(['success' => true,'projet'=> $projet]);
        }
        catch(\Exception $e)
        {
            DB::rollback();
            Log::info($e->getMessage());
            return response()->json(['success' => false,"message"=>$e->getMessage()],201);
        }
    }

    public function destroy(Projet $Projet)
    {
        //on supprime
        $Projet->delete();
        return response()->json(['message' => 'Projet supprimé avec succès'],200);
    }

    private function fillData(ProjetRequest $request,$hasClient = true)
    {
        return
        [

        ];
    }

    public function export()
    {

    }
}
