<?php

namespace App\Http\Controllers;

use App\Pays;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class PaysController extends Controller
{

            /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
      // $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $per = request()->query("per_page") && is_numeric(request()->query("per_page")) ? request()->query("per_page") : 10 ;
        $q = request()->query('filter') == null ? null : request()->query('filter');

        return  Pays::all();
           
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'libelle' => 'required|unique:roles|max:255',
        ]);

        Pays::create([
            'libelle' => $request->input('libelle'),
        ]);

        return response()->json([
            'message' => 'Pays ajoutée avec succès'],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pays  $pays
     * @return \Illuminate\Http\Response
     */
    public function show(Pays $pays)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pays  $pays
     * @return \Illuminate\Http\Response
     */
    public function edit(Pays $pays)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pays  $pays
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pays $pays)
    {

        
        $validatedData = $request->validate([
            'libelle' => 'required|max:255|unique:roles,libelle,'.$pays->id
        ]);
       
        $pays->libelle = $request->input('libelle');

        $pays->save();

        return response()->json([
            'message' => 'Pays modifié avec succès',
            'entity' => $pays],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pays  $pays
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pays $pays)
    {

        //on supprime
        $pays->delete();
        return response()->json([
            'message' => 'Pays supprimé avec succès',
            'entity' => $pays],200);

    }
}
