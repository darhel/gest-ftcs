<?php

namespace App\Http\Controllers;

use DB;
use Log;
use Excel;
use App\Task;

use App\Client;
use App\Projet;
use Carbon\Carbon;
use App\StatutPayement;
use Illuminate\Http\Request;
use App\Exports\LocationExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProjetRequest;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use App\Events\CashAdvancedHasBeenMadeEvent;

class ProjetController extends Controller
{
    public function index()
    {
        $per = request()->query("per_page") && is_numeric(request()->query("per_page")) ? request()->query("per_page"):10;
        // if(request()->expectsJSON())
        return   Projet::with(['entreprise','creatable','comments','tasks','fichiers'])->withCount('comments')->orderBy("created_at",'desc')->paginate($per);
    }

    public function create()
    {
        //
    }


    public function store(ProjetRequest $request)
    {
        try
        {

            DB::beginTransaction();

            if($request->input("description") != NULL){
                $description = $request->input("description");
            }else{
                $description = "Aucune description pour ce projet";
            }

            $projet = Projet::create(
                [
                    'titre' =>$request->input('titre'),
                    'type' =>$request->input("type"),
                    'lien' =>$request->input("lien"),
                    'resources' =>$request->input("resources"),
                    'date_debut_prev' =>$request->input("date_debut_prev"),
                    'date_fin_prev' =>$request->input("date_fin_prev"),
                    'avancement' =>$request->input('avancement'),
                    'responsable_id' =>$request->input("responsable_id"),
                    'entreprise_id' =>$request->input('entreprise_id'),
                    'description' => $description,
                    'creatable_type'=>"App\User",
                    'creatable_id'=>Auth::user()->id,
                ]
                );

            DB::commit();
            return ['success'=>true,'projet'=>$projet->load(['tasks','entreprise','creatable','comments','comments.user'])];

            // return response()->json(['success' => true,'projet'=> $projet->load(['entreprise','creatable','comments','tasks'])->withCount('comments')],200);
        }
        catch(\Exception $e)
        {
            DB::rollback();
            return ['status'=>false,'message'=>$e->getMessage()];
        }
    }


    public function show($id)
    {
        return Projet::with(['tasks','entreprise','creatable','comments','comments.user','fichiers'])->where('id',$id)->first();
    }

    public function edit()
    {

    }

    public function update(ProjetRequest $request, Projet $projet)
    {
        Gate::authorize('update', $projet);

        try
        {
            DB::beginTransaction();

            if($request->input("description") != NULL){
                $description = $request->input("description");
            }else{
                $description = "Aucune description pour ce projet";
            }

            $projet->titre = $request->input('titre');
            $projet->type = $request->input("type");
            $projet->type = $request->input("lien");
            $projet->resource = $request->input("resources");
            $projet->date_debut_prev = $request->input("date_debut_prev");
            $projet->date_fin_prev = $request->input("date_fin_prev");
            $projet->avancement = $request->input('avancement');
            $projet->responsable_id = $request->input("responsable_id");
            $projet->entreprise_id = $request->input('entreprise_id');
            $projet->description = $description;
            $projet->creatable_type = "App\User";
            $projet->creatable_id= Auth::user()->id;

            $projet->save();

            DB::commit();
            return ['success'=>true,'projet'=>$projet->load(['tasks','entreprise','creatable','comments','comments.user'])];
        }
        catch(\Exception $e)
        {
            DB::rollback();
            Log::info($e->getMessage());
            return response()->json(['success' => false,"message"=>$e->getMessage()],201);
        }
    }

    public function destroy(Projet $Projet)
    {
        Gate::authorize('delete', $Projet);
        //on supprime
        $Projet->delete();
        return response()->json(['message' => 'Projet supprimé avec succès'],200);
    }

    private function fillData(ProjetRequest $request,$hasClient = true)
    {
        return
        [

        ];
    }

    public function export()
    {

    }


}
