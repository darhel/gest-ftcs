<?php

namespace App\Http\Controllers;

use App\fichier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class fichierController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try
        {

            DB::beginTransaction();

        //  validation du formulaire
         request()->validate([
            'title'=>'required',
            'file'=>'required',
        ]);
        
        $photos = $request->file('file');

        if(!is_array($photos)){
            $photos = [$photos];
        }

        for($i = 0; $i < count($photos); $i++){
            $photo = $photos[$i];

            $imagesName = $photo->getClientOriginalName();
            $photo->move(storage_path('PiecesJointes'), $imagesName);
        }

            
        DB::commit();
        return ['success'=>true];
        }
        catch(\Exception $e)
        {
            DB::rollback();
            return ['status'=>false,'message'=>$e->getMessage()];
        }


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Fichier $fichier)
    {
        $fichier->delete();
        return response()->json(['message' => 'fichier supprimé avec succès'],200);
    }
}
