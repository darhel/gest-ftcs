<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CarteGriseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           "date_etablissement"=> 'required' ,
           'proprietaire' => 'required' ,
           'profession' => 'required' ,
           'adresse' => 'required' ,
           'fichier' => 'required' ,
           'vehicule_id' =>"required|exists:vehicules,id" ,

        ];
    }

    public function messages()
    {
        return
        [
            "date_etablissement.required"=>"La date d'établissement est requise",
            'date_etablissement.date' =>"La date d'établissement n'est pas valide",
            'vehicule_id.required' =>"Le véhicule à assurer est requis",
            'assureur_id.exists' =>"Cet assureur est inconnu",
            'proprietaire.required' =>"Le nom du propriétaire est requis",
            'profession.required' =>"La profession du propriétaire est requise",
            'adresse.required' =>"L'adresse du propriétaire est requise",
            'fichier.required' =>"Veuillez charger une copie de la carte grise",
        ];
    }
}
