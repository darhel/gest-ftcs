<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {


            return [
                'username' => "required|min:2|unique:users,username,".$this->input("id"),
                'name' => "required|min:2",
                'email' => "required|email|unique:users,email,".$this->input("id"),
                'role_id' =>  "required|exists:roles,id",
                'password' =>  "required|string|min:8|confirmed",
            ];


    }

        /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function messages()
    {


            return [
                'username.required' => "Le pseudo est obligatoire.",
                'username.unique' => "Le pseudo doit être unique.",
                'name.required' => "Le nom est obligatoire.",
                'email.required' => "L'e-mail est obligatoire.",
                'role_id.required' =>  "Le rôle est obligatoire.",
                'role_id.exists' =>  "Ce rôle est inconnu.",
                'email.email' =>  "Le format de cet e-mail est invalide",
            ];


    }
}
