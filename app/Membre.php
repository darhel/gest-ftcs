<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class Membre extends Authenticatable implements MustVerifyEmail
{
    protected $guard ="web";

    use HasApiTokens, Notifiable;

    protected $guarded=[];
    protected $appends =["hasPhoto",'fullName','fullNameWithTitel'];

    public function getHasPhotoAttribute()
    {
        if($this->avatar == null) return false;
        return file_exists(storage_path("/app/public/featured/".$this->avatar)) ? true : false;
    }
    public function getFullNameAttribute()
    {
        return ucfirst($this->prenom)." ".ucfirst($this->nom);
    }

    public function getFullNameWithTitelAttribute()
    {
        return ucfirst($this->civilite)." ".ucfirst($this->prenom)." ".ucfirst($this->nom);
    }
    public function scopeSecteur($query, $q)
    {
        if ($q == null) return $query;
        return $query
                ->where('membres.secteur_activite_id',$q);
    }
    public function scopeSearch($query, $q)
    {
        if ($q == null) return $query;
        return $query
                ->orWhere('membres.nom', 'LIKE', "%{$q}%")
                ->orWhere('membres.prenom', 'LIKE', "%{$q}%")
                ->orWhere('membres.derniere_profession', 'LIKE', "%{$q}%")
                ->orWhere('membres.activite_actuelle', 'LIKE', "%{$q}%")
                ->orWhere('membres.phone1', 'LIKE', "%{$q}%")
                ->orWhere('membres.phone2', 'LIKE', "%{$q}%")
                ->orWhere('membres.email', 'LIKE', "%{$q}%")
                ->orWhere('membres.prenom', 'LIKE', "%{$q}%")
                ->orWhere('membres.statut_marital', 'LIKE', "%{$q}%");
                //->orWhere('roles.libelle', 'LIKE', "%{$q}%")
                //->join('roles', 'roles.id', '=', 'users.role_id');
    }

    public function scopeSearchAsParameter($query, $q)
    {
        if ($q == null) return $query;
        return $query->orWhere('membres.nom', 'LIKE', "%{$q}%")
                     ->orWhere('membres.prenom', 'LIKE', "%{$q}%");
    }

    public function scopePromotion($query, $q)
    {
        if ($q == null) return $query;
        return $query
                ->where('membres.promotion',$q);
    }

    public function nationalites()
    {
        return $this->belongsToMany("App\Pays", 'nationalite_membre', 'membre_id', 'nationalite_id');

    }

    public function competences()
    {
        return $this->belongsToMany("App\Competence", 'competence_membre', 'membre_id', 'competence_id');

    }

    // public function residence()
    // {
    //     return $this->belongsTo("App\Pays","pays_residence_id");

    // }

    public function formation()
    {
        return $this->belongsTo("App\Formation");

    }
    public function secteur()
    {
        return $this->belongsTo("App\SecteurActivite","secteur_activite_id");

    }


    public function comments()
    {
        return $this->hasMany('App\Comment', 'membre_id');
    }

        /**
     * Get all of the member's enterprises.
     */
    public function entreprises()
    {
        return $this->morphMany('App\Entreprise', 'creatable');
    }


    /**
     * Get all of the member's enterprises.
     */
    public function projets()
    {
        return $this->morphMany('App\Projet', 'creatable');
    }
    //Fichier
    public function fichiers()
    {
        return $this->hasMany('App\Fichier', 'membre_id');
    }
 /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    // protected $fillable = [
    //     'nom', 'prenom', 'password','email','civilite',"username","pays_residence_id","secteur_activite_id",'promotion'
    // ];

}
