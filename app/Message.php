<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $guard = [];

    public function user_from()
    {
        return $this->belongsTo('App\User','from_id');
    }
    public function user_to()
    {
        return $this->belongsTo('App\User','to_id');
    }
}
