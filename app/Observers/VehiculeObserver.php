<?php

namespace App\Observers;

use App\Vehicule;

class VehiculeObserver
{
    /**
     * Handle the vehicule "created" event.
     *
     * @param  \App\Vehicule  $vehicule
     * @return void
     */
    public function created(Vehicule $vehicule)
    {
        //
    }

    /**
     * Handle the vehicule "updated" event.
     *
     * @param  \App\Vehicule  $vehicule
     * @return void
     */
    public function updated(Vehicule $vehicule)
    {
        //
    }

    /**
     * Handle the vehicule "deleted" event.
     *
     * @param  \App\Vehicule  $vehicule
     * @return void
     */
    public function deleted(Vehicule $vehicule)
    {
        //on supprime les images liées à ce vehicules
    
    }

    /**
     * Handle the vehicule "restored" event.
     *
     * @param  \App\Vehicule  $vehicule
     * @return void
     */
    public function restored(Vehicule $vehicule)
    {
        //
    }

    /**
     * Handle the vehicule "force deleted" event.
     *
     * @param  \App\Vehicule  $vehicule
     * @return void
     */
    public function forceDeleted(Vehicule $vehicule)
    {
        //
    }
}
