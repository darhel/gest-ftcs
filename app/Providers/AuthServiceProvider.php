<?php

namespace App\Providers;

use App\Entreprise;
use Illuminate\Routing\Route;
use Laravel\Passport\Passport;
use App\Policies\EntreprisePolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
        'App\Entreprise' => 'App\Policies\EntreprisePolicy',
        'App\Comment' => 'App\Policies\CommentPolicy',
        'App\Projet' => 'App\Policies\ProjetPolicy',
        'App\User' => 'App\Policies\UserPolicy',
        'App\Task' => 'App\Policies\TaskPolicy',

        // Entreprise::class => EntreprisePolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::guessPolicyNamesUsing(function ($entreprise) {
            return EntreprisePolicy::class;
        });

        Gate::guessPolicyNamesUsing(function ($comment) {
            return CommentPolicy::class;
        });

        Gate::guessPolicyNamesUsing(function ($projet) {
            return ProjetPolicy::class;
        });

        Gate::guessPolicyNamesUsing(function ($user) {
            return UserPolicy::class;
        });

        Gate::guessPolicyNamesUsing(function ($task) {
            return TaskPolicy::class;
        });

        // Gate::before(function ($user, $permission) {

        //         return $user->permissions()->contains($permission) ? true : false;

        // });

      //  Passport::loadKeysFrom('/secret-keys/oauth');
    }
}
