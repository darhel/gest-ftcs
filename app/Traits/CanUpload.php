<?php

namespace App\Traits;

use App\Events\DocsOnVehicleCreated;
use App\Events\GalleryOnVehicleCreated;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;


trait CanUpload
{

    /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function upload($file,$type,$prefix,$fileName="")
    {
        //On determine le type de fichier 
        $extension = $this->pickExtension($type);
        $file = str_replace($type.",", '', $file);
        $file = str_replace(' ', '+', $file);
        $fileName = $fileName == "" ? $prefix."-".uniqid().'.'.$extension : $fileName;
        Storage::disk('local')->put($this->storage_path."/".$fileName, base64_decode($file));
        Log::info($fileName);

        event(new DocsOnVehicleCreated($this,$fileName));  
    }

        /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function uploadFeatured($file,$type,$obj,$fileName="")
    {
        $prefix = $obj->id;
        //On determine le type de fichier 
        $extension = $this->pickExtension($type);
        $file = str_replace($type.",", '', $file);
        $file = str_replace(' ', '+', $file);
        $fileName = $fileName == "" ? $prefix."-".uniqid().'.'.$extension : $fileName;
        Storage::disk('local')->put("public/featured/".$fileName, base64_decode($file));
         
        return $fileName;

    }
    public function uploadAvatar($file,$type,$fileName="")
    {
        //On determine le type de fichier 
        $extension = $this->pickExtension($type);
        $file = str_replace($type.",", '', $file);
        $file = str_replace(' ', '+', $file);
        $fileName = $fileName == "" ? uniqid().'.'.$extension : $fileName;
        Storage::disk('local')->put("public/featured/".$fileName, base64_decode($file));
         
        return $fileName;

    }

        /**
     * Show the application's login form.
     *
     * @return \Illuminate\Http\Response
     */
    public function uploadGallery($file,$type,$prefix)
    {
        //On determine le type de fichier 
        $extension = $this->pickExtension($type);
        $file = str_replace($type.",", '', $file);
        $file = str_replace(' ', '+', $file);
        $fileName = $prefix."-".uniqid().'.'.$extension;
        $img = Image::make($file);
        // resize image
        $img->fit(720, 600);
        $img->save(storage_path('app/public/images/'.$fileName));
        event(new GalleryOnVehicleCreated($this,$fileName));  
    }


    public function pickExtension($base64Type)
    {
      $extension = "";
      switch ($base64Type) 
      {//check image's extension
            case "data:image/jpeg;base64":
                $extension = "jpeg";
                break;
            case "data:image/jpg;base64":
                    $extension = "jpg";
                    break;
            case "data:image/png;base64":
                $extension = "png";
                break;
            case "data:application/pdf;base64":
            $extension = "pdf";
                break;
            default://should write cases for more images types
            $extension = "";
                break;
      }

      return $extension;
    }

    public function getMime($fileAsBase64)
    {
      $parts = explode(',',$fileAsBase64);
      return  $parts[0] ? $parts[0] : null;
    }
}
