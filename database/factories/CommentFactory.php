<?php

/* @var $factory \Illuminate\Database\Eloquent\Factory */

use App\User;
use App\Projet;
use Faker\Generator as Faker;

$factory->define(App\Comment::class, function (Faker $faker) {

    $membre = User::inRandomOrder()->first();
    $projet = Projet::inRandomOrder()->first();

    return [
        'membre_id' =>$membre ? $membre->id : factory(App\User::class),
        'projet_id' =>$projet ? $projet->id : factory(App\Projet::class),
        'content' => $faker->text($maxNbChars = 300),
    ];
});
