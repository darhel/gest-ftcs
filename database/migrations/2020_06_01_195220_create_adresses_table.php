<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('adresses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('rue')->nullable();
            $table->string('nr')->nullable();
            $table->string('bp')->nullable();
            $table->string('quartier')->nullable();
            $table->string('ville')->nullable();
            $table->unsignedBigInteger('pays_id')->nullable();

            $table->timestamps();

            $table->foreign('pays_id')->references('id')->on('pays')->onDelete('set null');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('adresses');
    }
}
