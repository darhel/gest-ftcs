<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProjetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projets', function (Blueprint $table) {
            $table->bigIncrements('id')->index();
            $table->string('titre');
            $table->unsignedBigInteger('responsable_id')->nullable();
            $table->unsignedBigInteger('entreprise_id')->nullable();
            $table->unsignedBigInteger('secteur_activite_id')->nullable();
            $table->text('description')->nullable();
            $table->text('type')->nullable();
            $table->text('lien')->nullable();
            $table->text('resources')->nullable();
            $table->date('date_debut_prev')->defautl(date('Y-m-d'));
            $table->date('date_fin_prev')->defautl(date('Y-m-d'));
            $table->integer('avancement')->default(0);
            $table->boolean('visible')->default(true);
            $table->enum('statut',['fermé',"ouvert"])->default("ouvert");
            $table->morphs('creatable');
            $table->timestamps();
            $table->foreign('responsable_id')->references('id')->on('users')->onDelete('set null');
            $table->foreign('entreprise_id')->references('id')->on('entreprises')->onDelete('set null');
            $table->foreign('secteur_activite_id')->references('id')->on('secteur_activites')->onDelete('set null');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projets');
    }
}
