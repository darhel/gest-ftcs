<?php

use App\User;
use App\Membre;
use App\Projet;
use App\Comment;
use Illuminate\Database\Seeder;

class CommentSeed extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $i = 0;
        for($i;$i<50;$i++)
        {

            $user = User::inRandomOrder()->take(1)->pluck('id');
            $projet = Projet::inRandomOrder()->take(1)->pluck('id');

            $comment = factory('App\User')->create();
            $comment->user()->attach($user);
            $comment->projet()->attach($projet);

            $user->save();

        }
    }
}
