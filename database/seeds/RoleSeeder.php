<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Config;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = Config::get('roles');

        foreach($roles as $role)
        {
            factory('App\Role')->create(["nom"=>$role['nom'], "libelle"=>$role['libelle']]);
        }
    }
}
