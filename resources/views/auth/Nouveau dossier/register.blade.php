@extends('layouts.register')

@section('content')
<register  inline-template v-cloak>
    <div class="login-box " >
      <div class="login-logo">
        <a href="javascript:void(0)">
          <img src="{{asset('img/logo_ftcs.png') }}" alt="" class="mt-5 mb-2">
        </a>
      </div>
      <!-- /.login-logo -->
      <div class="card" style="border-radius: 30px;">
        <div class="card-body login-card-body">
          <p class="login-box-msg">Créer un nouveau compte</p>

          <form method="POST"  action="/register" @submit.prevent="onSubmit()"
                                @reset.prevent="reset()"
                                @keydown="form.errors.clear($event.target.name)"
                                @change="form.errors.clear($event.target.name)">
            @csrf

            <div class="form-group row">
                <div class="col-md-3">
                    <select v-model="form.civilite" name="civilite" :class="['form-control form-control-sm', form.errors.has('civilite')? ' is-invalid' : '']" id="" placeholder="Civilité" required autofocus>
                        <option value=""></option>
                        <option :value="civilite" v-for="(civilite,index) in _civilites" :key="index">@{{ civilite }}</option>
                    </select>
                    <span class="m-form__help m--font-danger invalid-feedback " v-if="form.errors.has('civilite')" v-text="form.errors.get('civilite')"></span>
                </div>

                <div class="col-md-9">
                    <input id="nom" type="text" v-model="form.nom" :class="['form-control form-control-sm', form.errors.has('nom')? ' is-invalid' : '']" name="nom" placeholder="Nom" required autocomplete="nom" autofocus>
                    <span class="m-form__help m--font-danger invalid-feedback " v-if="form.errors.has('nom')" v-text="form.errors.get('nom')"></span>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <input id="prenom" v-model="form.prenom" type="text" :class="['form-control form-control-sm', form.errors.has('prenom')? ' is-invalid' : '']" placeholder="Prenom" required autocomplete="prenom">
                    <span class="m-form__help m--font-danger invalid-feedback " v-if="form.errors.has('prenom')" v-text="form.errors.get('prenom')"></span>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <input v-model="form.email" id="email" type="email" name="email" :class="['form-control form-control-sm', form.errors.has('email')? ' is-invalid' : '']" placeholder="Email" required autocomplete="email">
                    <span class="m-form__help m--font-danger invalid-feedback " v-if="form.errors.has('email')" v-text="form.errors.get('email')"></span>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <multi-select
                        no-ptions="Aucune promotion disponible" open-direction="bottom"
                        name="promotion"
                        v-model="form.promotion" :options="_promotions"
                        :show-labels="false" :searchable="true" :clear-on-select="false"
                        :close-on-select="true"  placeholder="Promotion" required
                        >
                    </multi-select>
                    <span class="m-form__help m--font-danger"  v-if="form.errors.has('promotion')" v-text="form.errors.get('promotion')"></span>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <select v-model="form.secteur" name="secteur" :class="['form-control form-control-sm', form.errors.has('secteur')? ' is-invalid' : '']" id="secteur" placeholder="Secteur" required autofocus>
                        <option value="">Secteur d'activité</option>
                        <option :value="secteur.id" v-for="(secteur) in _secteurs" :key="secteur.id">@{{ secteur.libelle }}</option>
                    </select>
                    <span class="m-form__help m--font-danger invalid-feedback " v-if="form.errors.has('secteur')" v-text="form.errors.get('secteur')"></span>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-12">
                    <select v-model="form.pays" name="pays" :class="['form-control form-control-sm', form.errors.has('pays')? ' is-invalid' : '']" id="pays" placeholder="Pays" required autofocus>
                        <option value="">Nationalité</option>
                        <option :value="pays.id" v-for="(pays) in _pays" :key="pays.id">@{{ pays.libelle }}</option>
                    </select>
                    <span class="m-form__help m--font-danger invalid-feedback " v-if="form.errors.has('pays')" v-text="form.errors.get('pays')"></span>

                </div>
            </div>


            <div class="form-group row">
                <div class="col-md-12">
                    <input id="password" v-model="form.password" type="password" :class="['form-control form-control-sm', form.errors.has('password')? ' is-invalid' : '']" name="password" placeholder="Mot de passe" required autocomplete="new-password">
                    <span class="m-form__help m--font-danger invalid-feedback " v-if="form.errors.has('password')" v-text="form.errors.get('password')"></span>
                </div>
            </div>

            <div class="form-group row">
                <div class="col-md-12">
                    <input id="password-confirm" v-model="form.password_confirmation" type="password" class="form-control form-control-sm" name="password_confirmation" required placeholder="Confirmer le mot de passe">
                </div>
            </div>

            <div class="form-group row mb-0">
                <div class="col-md-7">
                    <a href="{{ route('login') }}"><small>Déja inscrit? Connectez-vous</small></a>
                </div>
                <div class="col-md-5">
                    {{--  <button style="min-height: 38px;min-width:74px" type="submit" :class="['btn btn-primary btn-block mr-2',isLoading ? 'm-loader m-loader--sm m-loader--light m-loader--right' :'']" :disabled="form.errors.any()">@{{ isLoading ? "Login" : 'Login' }}</button>  --}}
                    <button style="min-height: 38px;min-width:74px" type="submit" :class="['btn btn-primary btn-block mr-2',isLoading ? 'm-loader m-loader--sm m-loader--light m-loader--right' :'']" :disabled="form.errors.any()">@{{ isLoading ? "Enregistrement..." : 'S\'enregister' }}</button>

                    {{--  <button type="submit" class="btn btn-primary">
                       S'inscrire
                    </button>  --}}
                </div>
            </div>
        </form>
          <!-- /.social-auth-links -->

        </div>
        <!-- /.login-card-body -->
      </div>
    </div>
</register>

@endsection
